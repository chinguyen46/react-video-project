import axios from 'axios';


const KEY = 'AIzaSyB1gfHwRhAPFJ1UJuBC5zkyOeizItwg8dE'; 

export default axios.create({
	baseURL: "https://www.googleapis.com/youtube/v3",
	params: {
		part: "snippet",
		maxResults: "5",
		key: KEY,
	}
});