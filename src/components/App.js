import React from 'react';
import SearchBar from './SearchBar.js';
import youtube from '../apis/youtube.js';
import VideoList from './VideoList.js';
import VideoDetail from './VideoDetail.js';

 
class App extends React.Component{
	
	state = {
		videos: [],
		selectedVideo: null
		  }; 
	

	componentDidMount(){
		this.onTermSubmit('dogs');
	}


	onTermSubmit = async (searchTerm) =>{ //arrow function is used to bind to event handler
		const response = await youtube.get('/search', {
			params: {
				q: searchTerm
			}
		});
		this.setState(
				{ 
			videos: response.data.items,
			selectedVideo: response.data.items[0]	
				}
		);
	};

	onVideoSelect = (video) =>{
		//update state on app class
		this.setState({ selectedVideo: video });
	};
	
	render(){
		return(
			<div className="ui container">
				<SearchBar onTermSubmit={this.onTermSubmit} />
				<div className="ui grid">
					<div className="ui row">
						<div className="eleven wide column">
							<VideoDetail video={this.state.selectedVideo} />
						</div>
						<div className="five wide column">
							<VideoList videos={this.state.videos} onVideoSelect={this.onVideoSelect} />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default App; 