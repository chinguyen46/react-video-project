import React from 'react';
import VideoItem from './VideoItem.js';

const VideoList = (props) =>{
	  
	const renderedVideos = props.videos.map((video) =>{
		return <VideoItem video={video} onVideoSelect={props.onVideoSelect} key={video.id.videoId}/>;
	});
	
	return (
	<div className="ui relaxed divided list">
		{renderedVideos}
	</div>
	)
};


export default VideoList; 